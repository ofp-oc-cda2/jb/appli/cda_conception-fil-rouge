```mermaid
sequenceDiagram
    Participant User
    Participant App
    Participant Bdd
    User->>+App: Crée un compte (POST username, email, mot de passe)
    App->>+Bdd: Enregistre l'utilisateur dans la bdd
    Bdd-->>+App: valide l'enregistrement
    App-->>+User: Affiche la vue de connexion
    User->>+App: Entre ses identifiants pour se connecter (POST route auth)
    App->>+Bdd: Vérifie les enregistrements
    Bdd-->>+App: Valide les enregistrements
    App-->>+App: Délivre le json web token
    App->>+App: Vérifie la validité du token
    Bdd-->>+User: Affiche les informations du profil
```
