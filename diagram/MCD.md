
```mermaid
classDiagram


class Note{
    id
    title
    content
    deadline
    creator
    color
    participants
    created_at
    updated_at
    newNote()
    editNote()
    deleteNote()
}

class User{
    id
    username
    email
    password
    involvement
    picture
    Login()
    Logout()
    Register()
    editProfile()
    deleteUser()
}

class Role{
    id
    name
}

class Notification{
    id
    from
    to
    note
}



class File{
    id
    name
    posted_at
    posted_by
    uploadFile()
    downloadFile()
    deleteFile()
}

class Picture{
    id
    posted_at
    user
    uploadPicture()
}

Note "*" --> "*" User
Role "1" --> "*" User
Notification "*" --> "*" User
File "*" --> "*" User
Picture "*" --> "1" User
