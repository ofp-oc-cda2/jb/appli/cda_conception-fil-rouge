# Conception

## Proposal


- Collaborative note app allowing users to consult, create, update and delete notes involving other users.
- Users has to create an account and log in to make modifications. Read only when logged out.
- Role User and role Admin.
- Note has a deadline, a title, a content + a state when pinned, a creator and participants. 
- Each user can upload a profile picture.
- Possibility to being notified when you are added or removed from a note or when someone update a note you're in.
- Possibility to being notified when note's deadline you're in is becoming short.
- Possibility for admins to pinn notes, making it more visible and impossible to delete for simple users.
- Possibility to upload files into a storage pannel.
- Possibility to send a mail to another user from the app.
- Sort notes depending on deadline, creator, pinned or not


## Features

- Account system
- User Board
- Admin Board
- File upload
- Notifications
- Crud
- Sorting
- Email trigger
- (Draggable items)?

## Technologies

- Vue3
- Quasar
- Nodejs
- Express
- Mongodb
- JWT
- (Passport-jwt)
- (Vuelidate)
- Multer

## Tasks



- [x] Admin Board => check role from token
- [ ] Upload profile picture
- [ ] Storage and file upload
- [ ] front side crud operations on users from adminboard
- [x] Fix note front side crud operations - todo update
- [x] Sign up pannel
- [x] User Board => user infos from token
